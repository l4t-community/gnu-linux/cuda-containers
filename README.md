# Available container

| Name      | Version   |
| llama     | 32.7.1    |
| ferret    | 32.7.1    |
# Run

Replace "\<CONTAINER\>" by the container you want to run from the list available above
```sh
docker run -it --rm --runtime nvidia --network host -e NVIDIA_REQUIRE_JETPACK="csv-mounts=all" gitlab-registry.azka.li/l4t-community/gnu-linux/cuda-containers:<CONTAINER>
```
